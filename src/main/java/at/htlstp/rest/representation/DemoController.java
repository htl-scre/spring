package at.htlstp.rest.representation;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    // GET hello/SCRE -> Hello SCRE
    @GetMapping("/hello/{user}")
    public String demo(@PathVariable String user) {
        return "Hello " + user;
    }

    // GET bye -> Bye bye love
    // GET bye?user=SCRE -> Bye SCRE
    @GetMapping("/bye")
    public String bye(@RequestParam(defaultValue = "bye love") String user) {
        return "Bye " + user;
    }
}
