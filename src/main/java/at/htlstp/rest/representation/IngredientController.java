package at.htlstp.rest.representation;

import at.htlstp.rest.domain.Ingredient;
import at.htlstp.rest.persistence.IngredientRepository;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("ingredients") // prefix
public class IngredientController {

    private final IngredientRepository ingredientRepository;

    public IngredientController(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @GetMapping()
    public List<Ingredient> getAll(@RequestParam(required = false) String title) {
        if (title == null)
            return ingredientRepository.findAll();
        return ingredientRepository
                .findAllByTitleContainingIgnoreCase(title);
    }

    @GetMapping("{id}")
    public Ingredient getOne(@PathVariable long id) {
        return ingredientRepository
                .findById(id)
                .orElseThrow();
    }

    // Validation failed for classes [at.htlstp.rest.domain.Ingredient] DURING PERSIST TIME
    // -> @Valid
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Ingredient save(@RequestBody @Valid Ingredient ingredient) {
        return ingredientRepository.save(ingredient);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Ingredient update(@PathVariable long id, @RequestBody @Valid Ingredient ingredient) {
        ingredient.setId(id);
        return ingredientRepository.save(ingredient);
    }
}
