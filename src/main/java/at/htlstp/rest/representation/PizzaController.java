package at.htlstp.rest.representation;

import at.htlstp.rest.domain.Pizza;
import at.htlstp.rest.persistence.PizzaRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public record PizzaController(PizzaRepository pizzaRepository) {

    @GetMapping("pizza")
    List<Pizza> all() {
        return pizzaRepository.findAll();
    }

    @GetMapping("pizza/{name}")
    List<Pizza> allWithIngredient(@PathVariable String name) {
        return pizzaRepository.findAllWith(name);
    }
}
