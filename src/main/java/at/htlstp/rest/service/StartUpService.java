package at.htlstp.rest.service;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
public class StartUpService implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Important startup things");
    }
}
