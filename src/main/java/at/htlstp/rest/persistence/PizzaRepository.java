package at.htlstp.rest.persistence;

import at.htlstp.rest.domain.Ingredient;
import at.htlstp.rest.domain.Pizza;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PizzaRepository extends JpaRepository<Pizza, Long> {

    @Query("""
            select p from Pizza p
            join p.ingredients ingredient
            where ingredient.title = :ingredientName
            """)
    List<Pizza> findAllWith(String ingredientName);
}