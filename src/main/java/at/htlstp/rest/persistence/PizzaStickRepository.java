package at.htlstp.rest.persistence;

import at.htlstp.rest.domain.PizzaStick;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PizzaStickRepository extends JpaRepository<PizzaStick, Long> {
}