package at.htlstp.rest.persistence;

import at.htlstp.rest.domain.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Long> {

    List<Ingredient> findAllByTitleContainingIgnoreCase(String title);

    @Query(value = """
            select ingredient from Ingredient ingredient
            """)
    List<Ingredient> noU();


}
