insert into ingredients(title)
values ('cheese'),
       ('tomatoes'),
       ('salami'),
       ('tuna'),
       ('onion'),
       ('garlic'),
       ('chili');

insert into pizza_sticks(title, ingredient_id)
values ('cheesy', 1),
       ('ketchup', 2),
       ('garlic special', 6);

insert into pizzas(title)
values ('Margarita'),
       ('Tuna'),
       ('Palermo');

insert into pizza_ingredients(pizza_id, ingredient_id)
values (1, 1),
       (1, 2),
       (2, 1),
       (2, 2),
       (2, 4),
       (2, 5),
       (3, 1),
       (3, 2),
       (3, 3),
       (3, 6),
       (3, 7);
